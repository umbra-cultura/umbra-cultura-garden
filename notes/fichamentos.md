---
id: vxwtye00z2vh5azfb4hmywf
title: Fichamentos
desc: Coleção de fichamentos de materiais de estudo
updated: 1689603906949
created: 1688934519968
---

Segue um modelo para a realização de fichamentos de livros e artigos.

# Título do Livro / Artigo

_Referência em modelo ABNT do livro/artigo lido_

[Rotina de estudos: 11 dicas para criar (e manter!) a sua](https://www.estudarfora.org.br/rotina-de-estudos/)

## Resumo do texto

Apresentar as ideias principais do texto, uma descrição textual com as suas próprias palavras.
Resumo de uma página.

## Lista de citações e comentários

Indicar citações (na norma da ABNT) que julgar relevantes, trechos que expressam pontos fundamentais do capítulo.

> Exemplo de citação

Exemplo de comentário.

## Pesquisa ampliada

Definições complementares, conceitos extraídos de outros autores e materiais adicionais pesquisados. Pode ser organizado em uma tabela.

## Considerações

Descrever a importância deste capítulo para a aprendizagem.

## Referências bibliográficas

Indicar as referências dos materiais utilizados, seguindo as normas da ABNT.

# Lista de fichamentos por autor:

- ## Álvaro Cunhal
  - **Artigos:**
    - [ ] [[fichamentos.A superioridade moral dos comunistas]]
  - **Livros:**
- ## Amílcar Cabral
  - **Artigos:**
    - [ ] [[fichamentos.Aplicar na prática os princípios do partido]]
  - **Livros:**
- ## Clara Zetkin
  - **Artigos:**
    - [ ] [[fichamentos.O trabalho comunista entre as mulheres]]
  - **Livros:**
- ## Joseph Stalin
  - **Artigos:**
    - [ ] [[fichamentos;Sobre a união das repúblicas soviéticas]]
  - **Livros:**
- ## Luiz Falcão
  - **Artigos:**
    - [ ] [[fichamentos.Nosso trabalho com as massas]]
  - **Livros:**
- ## Manoel Lisboa
  - **Artigos:**
    - [ ] [[fichamentos.O exército burguês pode e deve ser destruído]]
  - **Livros:**
- ## Nadejda Krupskaia
  - **Artigos:**
    - [ ] [[fichamentos.Como Lênin estudava Marx]]
  - **Livros:**
- ## Vladimir Lênin
  - **Artigos:**
    - [ ] [[fichamentos.A atitude dos socialistas em relação às guerras]]
  - **Livros:**
