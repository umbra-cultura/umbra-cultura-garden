---
id: 2guw2511q6t5ykfdohko4bd
title: Cooperativas
desc: ''
updated: 1688934866596
created: 1688934823157
---

# Uma tese sobre cooperativas revolucionárias na Unidade Popular

## Introdução

Na primeira plenária do diretório goiano da Unidade Popular pelo Socialismo (UP) da qual participei, realizada no dia 25 de março desse ano, eu perguntei sobre a posição do partido sobre cooperativas. À época eu era recém integrante do partido, e meu nível de estudo e de conhecimento estava (e ainda está) em estágios iniciais. De lá pra cá, me juntei aos grupos de trabalho de organização de eventos e de comunicação da UP e uma das tarefas que me foram delegadas foi justamente fazer uma publicação relativa à data do Dia Internacional do Cooperativismo, comemorada de maneira oficial no dia 4 de julho. Para realizar essa tarefa foi preciso resgatar um pouco da história do movimento cooperativista, e também conhecer um pouco mais à fundo a realidade do cooperativismo hoje no nosso estado, bem como também conhecer as críticas que nós, enquanto socialistas, levantamos historicamente em relação a este movimento da maneira como foi consolidado.

Entretanto, por se tratar apenas de uma publicação informativa em relação à data comemorativa, muito pouco deste trabalho de pesquisa pôde ser aproveitado nesta tarefa em particular. Por esse motivo, foi também identificada a necessidade de um trabalho mais aprofundado, que analize o histórico do movimento cooperativista pelo método do materialismo histórico dialético, bem como as suas especificidades em relação ao Brasil e ao estado de Goiás, bem como manifestações contra-hegemônicas deste mesmo cooperativismo em movimentos de luta popular, sendo o exemplo mais conhecido as cooperativas do Movimento dos Trabalhadores Rurais Sem Terra (MST), [que vêem no cooperativismo agrícola uma forma de resistência](https://mst.org.br/2022/09/13/o-cooperativismo-como-bandeira-na-luta-pela-reforma-agraria-popular).

A intenção deste trabalho é, portanto, contribuir para com a produção teórica da Unidade Popular, a partir da análise histórica e material deste movimento na nossa realidade, de modo a definir de que maneira o cooperativismo pode contribuir para a construção da luta revolucionária e a vitória da classe trabalhadora.

## 1. Sobre o movimento cooperativista

## 2. As cooperativas em Goiás

## 3. As cooperativas alternativas do MST

## 4. O cooperativismo sobre os olhos da luta socialista

## 5. As cooperativas revolucionárias

https://averdade.org.br/2023/06/quem-deve-sustentar-o-partido-e-a-revolucao/?fbclid=PAAaalDPcc5_RLwAXaN91xGzS8nSqy8D6rvmmnpZEemeMRGuwHM2PrhfGZZ04_aem_th_ASdKzfPCm4FaMVzcvAgMfK9JQiX-FgKUYXdiG3Jb_YdzamlszJiEg46JLb-2pP0aWhY
